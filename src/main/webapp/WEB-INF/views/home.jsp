<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@ page session="false" %>
<html>
<head>
	<title>Home</title>
</head>
<body>
<h1>
	Hello world!  
</h1>

<P>  The time on the server is ${serverTime}. </P>
	<shiro:guest>
		<p>Hey guest.</p>
	</shiro:guest>
	<shiro:principal/>
	<shiro:notAuthenticated>
		<p>notAuthenticated</p>
	</shiro:notAuthenticated>
</body>
</html>
