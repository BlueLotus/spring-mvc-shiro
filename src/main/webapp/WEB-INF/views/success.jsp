<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Success Page</title>
	</head>
	
	<body>
		RuRu!!<br />
		<shiro:principal/>
		<shiro:authenticated>
			<input type="submit" value="authed"></input>
		</shiro:authenticated>
		<shiro:hasRole name="admin">
			<input type="submit" value="admin"></input>
		</shiro:hasRole>
		<shiro:hasRole name="generaluser">
			<input type="submit" value="generaluser"></input>
		</shiro:hasRole>
		<shiro:hasPermission name="linux:update">
			<input type="submit" value="linux:update"></input>
		</shiro:hasPermission>
		<shiro:hasPermission name="linux:read">
			<input type="submit" value="linux:read"></input>
		</shiro:hasPermission>
	</body>
</html>