package com.shiro.demo.web.service;

import org.springframework.stereotype.Service;

@Service
public class RabbitService {
	
	public void doService() {
		System.out.println("Rabbit Service.");
	}

}
