package com.shiro.demo;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.shiro.demo.web.service.RabbitService;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	@Autowired
	private RabbitService rabbitService;
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		model.addAttribute("serverTime", formattedDate );
		
		return "home";
	}
	
	@RequestMapping(value = "login", method = RequestMethod.GET)
	public String login() {
		return "login";
	}
	
	@RequestMapping(value = "unauthorized", method = RequestMethod.GET)
	public String unauthorized() {
		System.out.println("unauthorized.");
		return "unauthorized";
	}
	
	@RequestMapping(value = "/loginprocess", method = RequestMethod.POST)
	public String loginProcess(@RequestParam("username") String userName, @RequestParam("password") String password) {

		logger.debug("username: {}, password: {}", userName, password);
		
		UsernamePasswordToken token = new UsernamePasswordToken(userName, password);
		token.setRememberMe(true);
		Subject currentUser = SecurityUtils.getSubject();
		currentUser.login(token);
		
		logger.debug("{} has admin: {}", currentUser.getPrincipal(), currentUser.hasRole("admin"));
		logger.debug("Is authenticated: {}", currentUser.isAuthenticated());
		
		return "success";
	}
	
	@RequestMapping(value = "/dosomething")
	/*@RequiresAuthentication
	@RequiresPermissions(("linux:create"))*/
	public String doSomething(@ModelAttribute("Auth") String auth) {
		rabbitService.doService();
		logger.debug("Is auth: {}", SecurityUtils.getSubject().isAuthenticated());
		return "success";
	}
	
	@RequestMapping(value = "/logout")
	public String logout() {
		System.out.println("logout.");
		return "login";
	}
	
}
